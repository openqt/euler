#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import numpy as np


WIDTH = 640
HEIGH = 480
NUM = 80

CELLS = 8

def build_image(images, filename):
    # black image
    canvas = np.zeros((HEIGH * NUM/CELLS, WIDTH * CELLS, 3), np.uint8)
    canvas[:] = (0, 0, 255)

    row = col = 0
    for n, f in enumerate(images[:NUM], 1):
        canvas[row:row+HEIGH, col:col+WIDTH] = f

        if n % CELLS == 0:
            row += HEIGH
            col = 0
        else:
            col += WIDTH

    cv2.imwrite(filename, canvas)
    return canvas


def capture_video():
    cap = cv2.VideoCapture(0)

    i = 0
    images = []
    while cap.isOpened():
        ret, frame = cap.read()
        if ret:
            if len(images) >= NUM:
                i += 1
                build_image(images, 'Z%03d.jpg'%i)
                images = []
            else:
                images.append(frame)

            # cv2.imshow('demo', frame)

        if cv2.waitKey(20) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':
    capture_video()

