#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import glob, random
import logging
import sys
logging.basicConfig(level=logging.DEBUG)


TOTAL = 28
COLS = 1080
ROWS = 1920
CELLS = 7

def build_image(images, filename):
    # black image
    canvas = np.zeros((ROWS * TOTAL/CELLS, COLS * CELLS, 3), np.uint8)
    canvas[:] = (255, 255, 255)

    row = col = 0
    for n, f in enumerate(images[:TOTAL], 1):
        data = cv2.imread(f)
        # logging.debug('{0}:{1}, {2}:{3}'.format(row, row+ROWS, col, col+COLS))
        canvas[row:row+data.shape[0], col:col+data.shape[1]] = data

        if n % CELLS == 0:
            row += ROWS
            col = 0
        else:
            col += COLS

    cv2.imwrite(filename, canvas)
    return canvas


if __name__ == '__main__':
    files = glob.glob(r'E:\letv\temp\pic\*.jpg')
    for i in range(455, 1000):
        try:
            samples = random.sample(files, TOTAL)
            build_image(samples, 'cells/P%04d.jpg' % (i+1))
            sys.stdout.write('.')
        except:
            logging.exception('')
            sys.stdout.write('x')

