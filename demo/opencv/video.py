# -*- encoding: utf-8 -*-

import cv2

cv2.namedWindow('demo')

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', -1, 20.0, (640, 480))

cap = cv2.VideoCapture(0)
while cap.isOpened():
    r, f = cap.read()
    f = cv2.flip(f, 1)
    f = cv2.cvtColor(f, cv2.COLOR_RGB2GRAY)

    out.write(f)
    cv2.imshow('demo', f)
    if cv2.waitKey(50) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
