# -*- encoding: utf-8 -*-

from matplotlib import pyplot as plt
import networkx as nx

N = {
    'a': set('bcdef'),
    'b': set('ce'),
    'c': set('d'),
    'd': set('e'),
    'e': set('f'),
    'f': set('cgh'),
    'g': set('fh'),
    'h': set('fg'),
}

G = nx.DiGraph(N)
nx.draw(G, with_labels=True,
        node_color='r',
        edge_color='b',
        font_color='g',
        font_size=16)

plt.axis('off')
plt.savefig("labels_and_colors.png")  # save as png
plt.show()  # display
