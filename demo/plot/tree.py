# -*- encoding: utf-8 -*-

from matplotlib import pyplot as plt
import networkx as nx

T = nx.balanced_tree(2, 3)
nx.draw(T, with_labels=True)
plt.savefig('tree.png')
plt.show()
