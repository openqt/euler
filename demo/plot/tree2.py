# -*- encoding: utf-8 -*-

from ete2 import Tree, TreeStyle

T = Tree('((A,B),(C),(D,(E,F)));')

style = TreeStyle()
style.mode = 'c'
style.show_scale = False
style.arc_start = -180 # 0 degrees = 3 o'clock
style.arc_span = 180

T.render('tree2.png', w=300, h=300, units='px', tree_style=style)
T.show(tree_style=style)
