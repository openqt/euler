import random
import numpy as np

class Bunch(dict):
    def __init__(self, **kwargs):
        super(Bunch, self).__init__(**kwargs)
        self.__dict__ = self

# x = Bunch(name="Jayne Cobb", position="Public Relations")
# print x.name

def show(title, old_data, new_data):
    print '> [{0}]'.format(title)
    print old_data
    print new_data
    print '-'*30


data = [random.randrange(1000) for i in range(10)]

def gnome_sort(seq):
    i = 0
    while i < len(seq):
        if i == 0 or seq[i-1] < seq[i]:
            i += 1
        else:
            seq[i-1], seq[i] = seq[i], seq[i-1]
            i -= 1
    return seq
show('gnome', data, gnome_sort(data[:]))


def insertion_sort(seq):
    for i in range(1, len(seq)):
        while i > 0 and seq[i-1] > seq[i]:
            seq[i-1], seq[i] = seq[i], seq[i-1]
            i -= 1
    return seq
show('insertion', data, insertion_sort(data[:]))


def insertion_sort_rec(seq, i):
    if i == 0: return
    insertion_sort_rec(seq, i-1)
    while i > 0 and seq[i] < seq[i-1]:
        seq[i], seq[i-1] = seq[i-1], seq[i]
        i -= 1
    return seq
show('insertion rec', data, insertion_sort_rec(data[:], len(data)-1))


def selection_sort(seq):
    for i in range(len(seq)-1, 0, -1):
        max_j = i
        for j in range(i):
            if seq[j] > seq[max_j]:
                max_j = j
        seq[i], seq[max_j] = seq[max_j], seq[i]
    return seq
show('selection', data, selection_sort(data[:]))


def selection_sort_rec(seq, i):
    if i == 0: return
    max_j = i
    for j in range(i):
        if seq[j] > seq[max_j]:
            max_j = j
    seq[i], seq[max_j] = seq[max_j], seq[i]
    selection_sort_rec(seq, i-1)
    return seq
show('selection rec', data, selection_sort_rec(data[:], len(data)-1))


def mergesort(seq):
    mid = len(seq)//2
    lft, rgt = seq[:mid], seq[mid:]
    if len(lft) > 1: lft = mergesort(lft)
    if len(rgt) > 1: rgt = mergesort(rgt)
    res = []
    while lft and rgt:
        if lft[-1] >= rgt[-1]:
            res.append(lft.pop())
        else:
            res.append(rgt.pop())
    res.reverse()
    return (lft or rgt) + res


def native_max_perm_rec(seats, people):
    print 'people = ', people
    if len(people) == 1: return 1

    wants = set(seats[i] for i in people)
    print 'wants = ', wants

    if people != wants:
        return native_max_perm_rec(seats, wants)
    return people
M = [2, 2, 0, 5, 3, 5, 7, 4]
print native_max_perm_rec(M, range(len(M)))


def native_max_perm(seats, people):
    wants = set(seats)
    while len(people) > 1 and people != wants:
        people = wants
        wants = set(seats[i] for i in people)
    return people
print native_max_perm(M, range(len(M)))

from collections import defaultdict
def counting_sort(seq):
    print seq
    ret, save = [], defaultdict(list)
    for i in seq:
        save[i].append(i)
    for i in range(min(save), max(save)+1):
        ret.extend(save[i])
    return ret
print counting_sort([random.randrange(1000) for i in range(10)])


def native_celebrity(G):
    for u in range(len(G)):
        for v in range(len(G)):
            if u != v:
                if G[u][v] or not G[v][u]:
                    break
        else:
            return u
    return None


def native_celebrity2(G):
    u, v = 0, 1
    for i in range(2, len(G)+1):
        if G[v][u]: v = i
        else: u = i
    if u == len(G): i = v
    else: i = u
    for u in range(len(G)):
        if u != i:
            if G[i][u] or not G[u][i]:
                break
    else:
        return i
    return None

N = 10
G = np.ndarray((N, N), dtype=int, buffer=np.array([random.randrange(2) for i in range(N*N)]))
c = 5
for i in range(N):
    G[c][i] = 0
    G[i][c] = 1
print G
print 'monkey is', native_celebrity(G)
print 'monky2 is', native_celebrity2(G)

def native_topsort(G, S=None):
    if not S: S = set(G)
    if len(S) == 1: return list(S)

    v = S.pop()
    seq = native_topsort(G, S)

    min_j = 0
    for i, u in enumerate(seq):
        if v in G[u]: min_j = i
    seq.insert(min_j + 1, v)
    return seq


def counting_in(G):
    C = dict((i, 0) for i in G)
    for u in G:
        for v in G[u]:
            C[v] += 1
    return C

def topsort(G):
    C = counting_in(G)
    O = [i for i in C if C[i] == 0]
    S = []

    while O:
        u = O.pop()
        S.append(u)

        for v in G[u]:
            C[v] -= 1
            if C[v] == 0:
                O.append(v)
    return S

def radix_sort(seq):
    pass


def bucket_sort(seq):
    pass


