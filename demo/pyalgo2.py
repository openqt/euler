import json
import random

a, b, c, d, e, f, g, h = 'abcdefgh'
G = {
    a: {b, c, d, e, f},
    b: {c, e},
    c: {d},
    d: {e},
    e: {f},
    f: {c, g, h},
    g: {f, h},
    h: {f, g},
}

def walk(G, s, S=set()):
    P, Q = dict(), set()
    P[s] = None
    Q.add(s)
    while Q:
        u = Q.pop()
        for v in G[u].difference(P, S):
            Q.add(v)
            P[v] = u
    return P

print walk(G, a)


def tree_walk(T, r):
    for u in T[r]:
        tree_walk(T, u)


def rec_dfs(G, s, S=None):
    if S is None: S = set()
    S.add(s)
    for u in G[s]:
        if u not in S:
            rec_dfs(G, u, S)


def iter_dfs(G, s):
    S, Q = set(), []
    Q.append(s)
    while Q:
        u = Q.pop()
        if u not in S:
            S.add(u)
            Q.extend(G[u])
            yield u

print list(iter_dfs(G, a))


def traverse(G, s, qtype=set):
    S, Q = set(), qtype()
    Q.add(s)
    while Q:
        u = Q.pop()
        if u in S: continue
        S.add(u)

        for v in G[u]:
            Q.add(v)
        yield u

class stack(list):
    add = list.append

print list(traverse(G, a, stack))

def dfs(G, s, t, d, f, S=None):
    if S is None: S = set()

    d[s] = t; t += 1
    S.add(s)
    for u in G[s]:
        if u in S: continue
        t = dfs(G, u, t, d, f, S)
    f[s] = t; t += 1
    return t


def topsort(G):
    S, Q = set(), []
    def dfs_inner(G, u):
        for v in G[u]:
            if v not in S:
                S.add(v)
                dfs_inner(G, v)
        if u not in Q:
            Q.append(u)
    for u in G:
        dfs_inner(G, u)
    return reversed(Q)
print list(topsort(G))

def topsort2(G):
    S, Q = set(), []
    def dfs_inner(u):
        if u in S: return
        S.add(u)
        for v in G[u]:
            dfs_inner(v)
        Q.append(u)
    for u in G:
        dfs_inner(u)
    return reversed(Q)
print list(topsort2(G))

