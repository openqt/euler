import json, pprint
import random
from collections import *

a, b, c, d, e, f, g, h, i = 'abcdefghi'
G = {
    a: {b, c},
    b: {d, e, i},
    c: {d},
    d: {a, h},
    e: {f},
    f: {g},
    g: {e, h},
    h: {i},
    i: {h},
}

G2 = {
    a: {b, f},
    b: {c, d, f},
    c: {d},
    d: {e, f},
    e: {f},
    f: {},
}
# pprint.pprint(G)
# pprint.pprint('-'*30)

def iddfs(G, s):
    yielded = []
    steps = []
    def recurse(G, s, d, S=None):
        if s not in yielded:
            yielded.append(s)
            steps.append(d)
        if d == 0: return
        if S is None: S = set()
        if s not in S:
            S.add(s)

        for u in G[s]:
            if u not in S:
                recurse(G, u, d-1, S)

    n = len(G) - 1
    for d in range(n):
        if len(yielded) == n:
            print 'END in {0}'.format(d)
            break
        recurse(G, s, d)

    return yielded, steps

print 'iddfs', iddfs(G, a)


def bfs(G, s):
    Q, S = deque([s]), {s:None}
    while Q:
        u = Q.popleft()
        for v in G[u]:
            if v not in S:
                S[v] = u
                Q.append(v)
    return S
print 'bfs', bfs(G, a)

def tr(G):
    T = {}
    for u in G:
        T[u] = set()
    for u in G:
        for v in G[u]:
            T[v].add(u)
    return T
# pprint.pprint(tr(G))

def topsort(G):
    M = dict((u, 0) for u in G)
    for u in G:
        for v in G[u]:
            M[v] += 1
    Q = [u for u in G if M[u] == 0]
    S = []
    while Q:
        u = Q.pop()
        S.append(u)
        for v in G[u]:
            M[v] -= 1
            if M[v] <= 0: Q.append(v)
    return S
print 'topsort', topsort(G)

def walk(G, s, S=set()):
    P = {s:None}
    Q = set(s)
    while Q:
        u = Q.pop()
        for v in G[u]:
            if v not in P and v not in S:
                Q.add(v)
                P[v] = u
    return P
print 'walk', walk(G2, a)

# def dfs(G, s, S=set()):
#     S.add(s)
#     for u in G[s]:
#         if u not in S:
#             dfs(G, u, S)
#
# def dfs2(G, s):
#     S = set()
#     Q = [s]
#     while Q:
#         u = Q.pop()
#         if u not in S:
#             S.add(u)
#             Q.extend(G[u])

def dfs_topsort(G):
    Q, S = [], set()
    def recurse(u):
        if u in S: return
        S.add(u)
        for v in G[u]:
            recurse(v)
        Q.append(u)

    for u in G:
        recurse(u)
    Q.reverse()
    return Q
print 'dfs topsort', dfs_topsort(G)

def scc(G):
    T = tr(G)
    sccs, seen = [], set()
    # for u in dfs_topsort(G):
    for u in G:
        if u in seen: continue
        C = walk(T, u, seen)
        seen.update(C)
        sccs.append(C)
    return sccs
print 'scc', scc(G)

def scc2(G):
    T = tr(G)
    sccs, seen = [], set()
    for u in reversed(dfs_topsort(G)):
        if u in seen: continue
        C = walk(G, u, seen)
        seen.update(C)
        sccs.append(C)
    return sccs
print 'scc2', scc2(G)


