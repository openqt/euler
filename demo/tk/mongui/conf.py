# -*- encoding: utf-8 -*-
__author__ = 'Jack Chu'

import sys, os
import logging


MONGO_URI = ['mongodb://localhost:27017',]

DEBUG = 'DEBUG'
INFO = 'INFO'
WARN = 'WARN'

def init(callback):
    logging.basicConfig(format='%(levelname)s:%(asctime)s:%(message)s',
                        level=logging.DEBUG)
    self = sys.modules[__name__]
    setattr(self, '_callback', callback)


def debug(s):
    logging.debug(s)
    _callback('DEBUG', s)


def info(s):
    logging.info(s)
    _callback('INFO', s)


def warn(s):
    logging.warn(s)
    _callback('WARN', s)


cf = os.environ.get('MONGUI_CONF')
if cf:
    with open(cf) as f:
        exec(compile(f.read(), cf, 'exec'), globals())
