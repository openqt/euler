# -*- encoding: utf-8 -*-
__author__ = 'Jack Chu'

import Tkinter as tk
import pymongo as mg
import traceback
import conf, view, json
import re
from datetime import datetime
import logging

class Model(object):
    """MongoDB model layer"""

    def __init__(self, uri):
        self.client = mg.MongoClient(uri, read_preference=mg.ReadPreference.PRIMARY_PREFERRED)

    def database_names(self):
        """:return database names"""
        dbs = self.client.database_names()
        conf.debug(dbs)
        return dbs

    def db(self, name):
        """:return db instance"""
        return self.client[name]

    def collection_names(self, dbname):
        """:return collection names by given db name"""
        conf.debug('collections from {0}'.format(dbname))
        coll = self.client[dbname].collection_names()
        conf.debug(coll)
        return coll

    @staticmethod
    def collection(db, name):
        """return collection instance"""
        return db[name]

    @staticmethod
    def select(coll, skip=0, limit=100):
        """select data set with skip and limit"""
        data = [i for i in coll.find().skip(skip).limit(limit)]
        # conf.debug(data)
        return data


class Client(object):
    def __init__(self, title):
        self.root = tk.Tk()
        self.root.title(title)
        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.root.geometry('1000x600+100+50')
        self.root.option_add('*tearOff', tk.FALSE)

        self.view = view.Main(self.root)

        self.view.dataview.find_callback = self.execute
        self.view.dataview.refresh_callback = self.select

        conf.init(self.logwindow)
        # conf.info('Tk: {0}'.format(tk.TkVersion))

        self.orm = {}  # tree node iid and db instance mappings
        self.last_selected = None

        for uri in conf.MONGO_URI:
            try:
                for i in ['@(.+)', '//(.+)']:
                    m = re.search(i, uri)
                    if m: break
                name = m.group(1)

                self.currentdb = Model(uri)
                self.orm[name] = self.currentdb
                self.fill_explorer(name)
            except:
                logging.exception('')

    def run(self):
        self.root.mainloop()


    def fill_explorer(self, name):
        """fill explorer with MongoDB and Collections info"""
        tree = self.view.explorer.collections
        tree.insert('', 'end', name, text=name)

        for dbname in self.currentdb.database_names():
            iid = view._iid(name, dbname)
            conf.debug(iid)
            tree.insert(name, 'end', iid, text=dbname, image=view.Icon.database)

            self.__fill_explorer_node(iid, 'Collections', dbname, view.Icon.folder, True)
            self.__fill_explorer_node(iid, 'JavaScript', dbname, view.Icon.folder)
            self.__fill_explorer_node(iid, 'GridFS', dbname, view.Icon.folder)
            self.__fill_explorer_node(iid, 'Users', dbname, view.Icon.user)

    def __fill_explorer_node(self, parentid, text, dbname, image='', deep=False):
        """fill single node with special type information"""
        tree = self.view.explorer.collections

        iid = parentid + '_' + text
        conf.debug(iid)
        tree.insert(parentid, 'end', iid, text=text, tags=iid, image=image)
        # tree.tag_bind(iid, '<<TreeviewSelect>>', callback=self.expand)

        if deep:
            self.orm[iid] = self.currentdb.db(dbname)

            for cname in self.currentdb.collection_names(dbname):
                iid2 = iid + '_' + cname
                conf.debug(iid2)

                self.orm[iid2] = Model.collection(self.orm[iid], cname)
                tree.insert(iid, 'end', iid2, text=cname, tags=iid2)
                tree.tag_bind(iid2, '<<TreeviewSelect>>', callback=self.select)


    def expand(self, _=None):
        tree = self.view.explorer.collections
        iid = tree.selection()[0]
        tree.item(iid, open=True)


    def select(self, _=None):
        """select collection and show data"""
        tree = self.view.explorer.collections
        iid = tree.selection()[0]
        conf.info(iid)

        if not _ or iid != self.last_selected:
            self.last_selected = iid
            conf.debug('fill dataview')
            self.view.dataview.fill(Model.select(self.orm[iid]))


    def execute(self, s):
        try:
            item = self.view.explorer.collections.focus()
            conf.debug(item)

            db = self.orm[item]
            conf.debug(db)

            self.view.dataview.fill([i for i in db.find(json.loads(s))])
        except:
            conf.info(traceback.format_exc(1))


    def logwindow(self, tag, s):
        if isinstance(s, (list, tuple)): s = ''.join(s)
        msg = '{0}:{1} - {2}\n'.format(tag, datetime.now(), s)

        if self.view.logview is None: return
        if tag == 'INFO':
            self.view.logview.info_(msg)
        elif tag == 'WARN':
            self.view.logview.info_(msg)
        else:
            self.view.logview.info_(msg)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    w = Client('MongoDB Client')
    w.run()
