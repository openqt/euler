# -*- encoding: utf-8 -*-
__author__ = 'Jack Chu'

import Tkinter as tk
import ttk
import bson.json_util as json
import ImageTk, re


DEFAULT_FONT = 'Consolas 10'

def grid(widget, column, row, columnspan=1):
    widget.grid(column=column, row=row, columnspan=columnspan, sticky='news')
    widget.master.columnconfigure(column, weight=1)
    widget.master.rowconfigure(row, weight=1)
    return widget


def default_grid(widget, column=0, row=0, columnspan=1, rowspan=1, weight=True, sticky='news'):
    """set widget to default grid layout"""
    widget.grid(column=column, row=row, columnspan=columnspan, rowspan=rowspan, sticky=sticky)
    if weight:
        widget.columnconfigure(column, weight=1)
        widget.rowconfigure(row, weight=1)
    return widget


def vsb(widget, column=1, row=0):
    """add vertical scrollbar"""
    vs = ttk.Scrollbar(widget, orient=tk.VERTICAL, command=widget.yview)
    vs.grid(column=column, row=row, sticky='ns')
    widget.configure(yscrollcommand=vs.set)


def _iid(a, b):
    """generate item iid"""
    return str(a) + '_' + str(b)


def _type(t):
    """simple type converter"""
    m = re.search("'([^']+)", repr(type(t)))
    return m.group(1) if m else ''


class Icon(object):
    def __init__(self):
        Icon.connect = ImageTk.PhotoImage(file='image/connect.png')
        Icon.refresh = ImageTk.PhotoImage(file='image/refresh.png')
        Icon.export = ImageTk.PhotoImage(file='image/export.png')
        Icon.find = ImageTk.PhotoImage(file='image/search.png')
        Icon.collection = ImageTk.PhotoImage(file='image/collection.png')
        Icon.folder = ImageTk.PhotoImage(file='image/folder.png')
        Icon.javascript = ImageTk.PhotoImage(file='image/javascript.png')
        Icon.user = ImageTk.PhotoImage(file='image/user.png')
        Icon.back = ImageTk.PhotoImage(file='image/back.png')
        Icon.forward = ImageTk.PhotoImage(file='image/forward.png')
        Icon.empty = ImageTk.PhotoImage(file='image/empty.png')
        Icon.database = ImageTk.PhotoImage(file='image/database.png')


class Explorer(ttk.Frame):
    """Database Explorer"""

    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)
        self.grid(column=0, row=0, rowspan=3, sticky='news')

        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

        self.connect = default_grid(ttk.Button(self,
                                               text='Connect',
                                               image=Icon.connect,
                                               compound='left'), weight=False)
        self.refresh = default_grid(ttk.Button(self,
                                               text='Refresh',
                                               image=Icon.refresh,
                                               compound='left'), column=1, weight=False)
        self.collections = default_grid(ttk.Treeview(self), row=1, columnspan=2)
        self.collections.heading('#0', text='Database Explorer')

        vsb(self.collections, row=1)


class Dataview(ttk.Frame):
    """Show json data in different views"""

    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)
        self.grid(column=1, row=0, columnspan=3, rowspan=3, sticky='news')

        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)

        self.limitvalue = tk.StringVar(value='1, 100')

        self.prev = default_grid(ttk.Button(self, text='Previous', image=Icon.back), column=3, weight=False)
        self.limit = default_grid(tk.Entry(self, font=DEFAULT_FONT, textvariable=self.limitvalue), column=4, weight=False, sticky='')
        self.next = default_grid(ttk.Button(self, text='Next', image=Icon.forward), column=5, weight=False)
        self.next = default_grid(ttk.Label(self, text='  '), column=6, weight=False)

        self.findbtn = default_grid(ttk.Button(self,
                                            text='Find',
                                            image=Icon.find,
                                            compound='left',
                                            command=self.call_find), column=7, weight=False)
        self.refreshbtn = default_grid(ttk.Button(self,
                                               text='Refresh',
                                               image=Icon.refresh,
                                               compound='left',
                                               command=lambda: self.refresh_callback()), column=8, weight=False)
        self.exportbtn = default_grid(ttk.Button(self,
                                              text='Export',
                                              image=Icon.export,
                                              compound='left'), column=9, weight=False)
        self.notebook = default_grid(ttk.Notebook(self), row=1, columnspan=10, weight=False)

        self.views = []
        for i, n in enumerate(['Tree View', 'Table View', 'Text View']):
            frame = default_grid(ttk.Frame(self.notebook, height=500, width=800))
            self.notebook.add(frame, text=n)
            self.views.append(frame)

        self.treeview = default_grid(ttk.Treeview(self.views[0], columns=('Value', 'Type')))
        self.treeview.heading('#0', text='Property')
        self.treeview.heading('Value', text='Value')
        self.treeview.heading('Type', text='Type')
        self.treeview.column('#0', width=100, anchor='w')
        self.treeview.column('Type', width=100, anchor='w')
        # self.treeview.tag_configure('font', font=DEFAULT_FONT)

        self.textview = default_grid(tk.Text(self.views[2], font=DEFAULT_FONT))
        self.findwindow = None

        vsb(self.treeview)
        vsb(self.textview)
        self.menu()


    def fill(self, data):
        """fill json object to ttk.Treeview"""
        self.treeview.delete(*self.treeview.get_children())
        for n, i in enumerate(data):
            iid = self.__fill_treeview('', '({0})'.format(n), i)
            self.treeview.tag_configure(iid, background='lightblue')

        self.__fill_textview(data)


    def __fill_treeview(self, iid, key, value):
        """fill data by types"""
        _id = _iid(iid, key)
        if isinstance(value, (list, tuple)):
            self.treeview.insert(iid, 'end', _id,
                                 text=key + ' [{0}]'.format(len(value)),
                                 tags=(_id, 'font'),
                                 values=('', _type(value)))
            for n, i in enumerate(value):
                self.__fill_treeview(_id, '({0})'.format(n), i)
        elif isinstance(value, dict):
            self.treeview.insert(iid, 'end', _id,
                                 text=key + ' {..}',
                                 tags=(_id, 'font'),
                                 values=('', _type(value)))
            for k, v in value.items():
                self.__fill_treeview(_id, k, v)
        else:
            self.treeview.insert(iid, 'end', _id,
                                 text=key,
                                 tags=(_id, 'font'),
                                 values=(value, _type(value)))
        return _id


    def __fill_textview(self, data):
        self.textview.delete(1.0, tk.END)
        self.textview.insert(1.0, json.dumps(data, indent=4, sort_keys=True))


    def menu(self):
        self.tree_menu = tk.Menu(self.treeview)
        self.tree_menu.add_command(label='Copy Item', command=self.copy_tree_item)
        self.treeview.bind('<3>', lambda e: self.tree_menu.post(e.x_root, e.y_root))

        self.text_menu = tk.Menu(self.textview)
        self.text_menu.add_command(label='Copy', command=self.copy_text)
        self.textview.bind('<3>', lambda e: self.text_menu.post(e.x_root, e.y_root))


    def copy_tree_item(self):
        item = self.treeview.focus()
        if item:
            data = self.treeview.item(item)
            self.clipboard_clear()
            self.clipboard_append('{{"{0}":"{1}"}}'.format(
                str(data['text']).encode('utf-8'), str(data['values'][0]).encode('utf-8')))


    def copy_text(self):
        text = self.textview.get('1.0', 'end')
        if text:
            self.clipboard_clear()
            self.clipboard_append(text)


    def call_find(self):
        try:
            self.findwindow.deiconify()
        except:
            self.findwindow = FindWindow(self, self.find_callback)


class Logview(ttk.Frame):
    """Log text window"""

    def __init__(self, parent):
        ttk.Frame.__init__(self, parent)
        self.grid(column=0, row=3, columnspan=4, sticky='news')

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.text = default_grid(tk.Text(self, height=5, font='Verdana 10'))
        self.text.tag_configure('DEBUG', foreground='gray')
        self.text.tag_configure('INFO', foreground='darkgreen')
        self.text.tag_configure('WARN', foreground='red')

        vsb(self.text, column=1, row=0)


    def info_(self, msg):
        self.text.insert('1.0', msg, 'INFO')


class Main(ttk.Frame):
    def __init__(self, parent):
        ttk.Frame.__init__(self, parent, width=800, height=600)
        default_grid(self)

        Icon()
        self.build()


    def build(self):
        pw1 = ttk.PanedWindow(self, orient=tk.HORIZONTAL)
        default_grid(pw1)

        self.explorer = Explorer(pw1)
        pw1.add(self.explorer)

        pw2 = ttk.PanedWindow(pw1, orient=tk.VERTICAL)
        default_grid(pw2, column=1)
        pw1.add(pw2)

        self.dataview = Dataview(pw2)
        pw2.add(self.dataview)

        self.logview = Logview(pw2)
        pw2.add(self.logview)


class FindWindow(tk.Toplevel):
    """Find window"""

    def __init__(self, parent, callback):
        tk.Toplevel.__init__(self, parent)
        self.title('Find')
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.callback = callback

        def _build(parent, text, height=3, width=50):
            frame = default_grid(ttk.LabelFrame(parent, text=text))
            widget = default_grid(tk.Text(frame, height=height, width=width, font=DEFAULT_FONT))
            parent.add(frame)
            return widget

        pw = default_grid(ttk.PanedWindow(self, orient=tk.HORIZONTAL))
        self.find = _build(pw, '{Find}')

        pw2 = default_grid(ttk.PanedWindow(pw, orient=tk.VERTICAL))
        pw.add(pw2)
        self.field = _build(pw2, '{Fields}', width=30)

        dash_frame = default_grid(ttk.Frame(pw2))
        pw2.add(dash_frame)

        sort_frame = default_grid(ttk.LabelFrame(dash_frame, text='{Sort}'), columnspan=2)
        default_grid(tk.Text(sort_frame, height=2, width=30, font=DEFAULT_FONT))

        self.findbtn = default_grid(ttk.Button(dash_frame, text='Find', command=self.execute), row=1, column=0)
        self.cancelbtn = default_grid(ttk.Button(dash_frame, text='Cancel', command=self.destroy), row=1, column=1)


    def execute(self):
        s = self.find.get('1.0', 'end').strip()
        if s:
            self.callback(s)
