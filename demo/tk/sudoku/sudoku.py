# -*- encoding: utf-8 -*-
import Tkinter as tk
import ttk, ImageTk
import sys, math, re, logging
import calc

MATRIX = 9
HALF = int(math.sqrt(MATRIX))
INRANGE = lambda x: '1' <= x <= '9'
def NUMPADGEN():
    for i in range(HALF):
        for j in range(HALF):
            yield i, j, str(i * HALF + j + 1)

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger()

root = tk.Tk()
root.title('Sudoku')
root.geometry('400x400+100+100')
root.option_add('*tearOff', tk.FALSE)
ttk.Style().configure('TButton', foreground='blue', font=('Consolas', 11), relief=tk.FLAT)
ttk.Style().configure('TLabel', foreground='darkgreen', font=('Consolas', 11, 'bold'), relief=tk.FLAT)
ttk.Style().configure('TSeparator', background='sienna')

numpad_window = 0

def grid(widget, column, row, columnspan=1):
    widget.grid(column=column, row=row, columnspan=columnspan, sticky='news')
    widget.master.columnconfigure(column, weight=1)
    widget.master.rowconfigure(row, weight=1)
    return widget

def select(btn):
    w = root.winfo_containing(*root.winfo_pointerxy())
    btn['text'] = w['text']
    numpad_window.destroy()

def click():
    global numpad_window
    if numpad_window: numpad_window.destroy()

    btn = root.winfo_containing(*root.winfo_pointerxy())
    numpad_window = tk.Toplevel(btn)
    [grid(ttk.Button(numpad_window, text=i*HALF+j+1, command=lambda: select(btn)), j, i) for i, j, t in NUMPADGEN()]
    grid(ttk.Button(numpad_window, text='', command=lambda: select(btn)), 0, HALF, HALF)
    numpad_window.geometry('60x120+{0}+{1}'.format(*root.winfo_pointerxy()))

def calc():
    calc.calc()

SLOTS = range(HALF, MATRIX, HALF+1)
MAP = [i for i in range(MATRIX+HALF-1) if i not in SLOTS]
with open(sys.argv[1]) as f:
    data = re.sub(r'[\r\n]', '', f.read())
    log.info(data)

for i in range(MATRIX):
    for j in range(MATRIX):
        n = i * MATRIX + j
        if INRANGE(data[n]):
            grid(ttk.Label(root, text=data[n], anchor='center'), MAP[j], MAP[i])
        else:
            grid(ttk.Button(root, text='', command=click), MAP[j], MAP[i])
for i in SLOTS:
    ttk.Separator(root, orient=tk.HORIZONTAL).grid(column=0, row=i, columnspan=MATRIX+HALF-1, sticky='we')
    ttk.Separator(root, orient=tk.VERTICAL).grid(column=i, row=0, rowspan=MATRIX+HALF-1, sticky='ns')

calc_img = ImageTk.PhotoImage(file='calculator.png')
exit_img = ImageTk.PhotoImage(file='exit.png')
menubar = tk.Menu(root)
menu_dash = tk.Menu(menubar)
menu_dash.add_command(label='Calculation', command=calc, image=calc_img, compound='left')
menu_dash.add_separator()
menu_dash.add_command(label='Exit', command=lambda: exit(0), image=exit_img, compound='left')
menubar.add_cascade(menu=menu_dash, label='Dashboard')
root['menu'] = menubar
root.mainloop()
