# -*- encoding: utf-8 -*-
__author__ = 'Jack Chu'

import Tkinter as tk
import tkFileDialog as filedialog
import tkMessageBox as messagebox
import ttk
import requests, json
from datetime import datetime
import ImageTk
import logging
import os, traceback

logging.basicConfig(level=logging.DEBUG)

class Model(object):
    """static cloud utility"""
    # ROOTURL = 'http://static.scloud.letv.com/'
    ROOTURL = 'http://internal.static.scloud.letv.com/'

    @staticmethod
    def req(method, url, data=None, file=None):
        logging.info(url)
        resp = requests.request(method, url, timeout=10, data=data, files=file)
        if resp.status_code != 200:
            raise Exception('STATUS CODE: {0}'.format(resp.status_code))
        return resp

    @staticmethod
    def buckets():
        resp = Model.req('GET', Model.ROOTURL)
        return json.loads(resp.content)

    @staticmethod
    def objects(bucket):
        resp = Model.req('GET', Model.ROOTURL + bucket)
        return sorted(json.loads(resp.content), key=lambda d: d['uploadDate'], reverse=True)

    @staticmethod
    def upload(bucket, file):
        with open(file, 'rb') as f:
            d, n = os.path.split(file)
            url = '{0}/{1}'.format(Model.ROOTURL + bucket, n)
            logging.info('UPLOAD {0}'.format(url))
            return Model.req('POST', url, file={n: f.read()})

    @staticmethod
    def download(bucket, file):
        url = '{0}/{1}'.format(Model.ROOTURL + bucket, file)
        logging.info('DOWNLOAD {0}'.format(url))
        resp = Model.req('GET', url)
        with open(file, 'wb') as f:
            f.write(resp.content)

    @staticmethod
    def delete(bucket, file):
        url = '{0}/{1}'.format(Model.ROOTURL + bucket, file)
        logging.info('DELETE {0}'.format(url))
        return Model.req('DELETE', url)


class View(object):
    """viewer"""
    def __init__(self, root):
        """setup widgets' layout"""
        root.title('scloud client')
        root.geometry('1000x600+100+50')
        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        root.option_add('*tearOff', tk.FALSE)

        self.view = ttk.Treeview(root, columns=('filename', 'uploadDate', 'length'))
        self.columnconf('#0', 'bucket', 60)
        self.columnconf('filename', 'filename', 250)
        # self.columnconf('md5', 'md5')
        # self.columnconf('chunkSize', 'chunkSize')
        self.columnconf('uploadDate', 'uploadDate')
        self.columnconf('length', 'length', 30)

        self.view.grid(column=0, row=0, sticky='news')
        self.view.columnconfigure(0, weight=1)
        self.view.rowconfigure(0, weight=1)

        self.vsb(self.view)
        self._context_menu()

    def columnconf(self, column, text, width=50):
        self.view.heading(column, text=text)
        self.view.column(column, width=width)

    def vsb(self, widget, column=1, row=0):
        """add vertical scrollbar"""
        vs = ttk.Scrollbar(widget, orient=tk.VERTICAL, command=widget.yview)
        vs.grid(column=column, row=row, sticky='ns')
        widget.configure(yscrollcommand=vs.set)

    def _context_menu(self):
        """add context menu items"""
        self.copy_img = ImageTk.PhotoImage(file='image/copy.png')
        self.delete_img = ImageTk.PhotoImage(file='image/delete.png')
        self.upload_img = ImageTk.PhotoImage(file='image/upload.png')
        self.download_img = ImageTk.PhotoImage(file='image/download.png')

        menu = tk.Menu(self.view)
        menu.add_command(label='Copy', command=lambda: self.copy(), image=self.copy_img, compound='left')
        menu.add_command(label='Delete', command=lambda: self.delete(), image=self.delete_img, compound='left')
        menu.add_separator()
        menu.add_command(label='Upload...', command=lambda: self.upload(), image=self.upload_img, compound='left')
        menu.add_separator()
        menu.add_command(label='View...', command=lambda: self.download(), image=self.download_img, compound='left')
        self.view.bind('<3>', lambda e: menu.post(e.x_root, e.y_root))

    def copy(self):
        """copy tree item to clipboard"""
        data = self.view.item(self.view.focus())
        self.view.clipboard_clear()
        self.view.clipboard_append(data['values'])

class Controller(object):
    def __init__(self):
        self.root = tk.Tk()
        self.folder_img = ImageTk.PhotoImage(file='image/bucket.png')

        self.view = View(self.root)
        self.view.upload = self.upload
        self.view.delete = self.delete
        self.view.download = self.download

        self.model = Model()

    def run(self):
        self.root.mainloop()

    @staticmethod
    def byte2N(n):
        for i in (' b', ' KB', ' MB', ' GB'):
            if n >= 1024: n /= 1024
            else: break
        return str(int(n)) + i

    def _focus(self):
        """return (bucket, file)"""
        tree = self.view.view
        item = tree.focus()
        if not item:
            messagebox.showinfo('INFO', u'先选择一个上传的文件夹')
            return None, None

        values = tree.item(item, 'values')

        tags = tree.item(item, 'tags')
        if not tags or tags[0] == 'DATA':
            item = tree.parent(item)
            tags = tree.item(item, 'tags')
        bucket = tags[0]

        if len(values) <= 0:
            file = ''
        else:
            file = values[0]

        logging.info('{0}/{1}'.format(bucket, file))
        return bucket, file, item


    def upload(self):
        b, _, i = self._focus()
        for f in filedialog.askopenfilenames():
            Model.upload(b, f)
        self._fill(i)

    def delete(self):
        b, f, i = self._focus()
        self.model.delete(b, f)
        self._fill(i)


    def download(self, _=None):
        try:
            b, f, _ = self._focus()
            self.model.download(b, f)
            os.startfile(f)
        except:
            messagebox.showinfo('exception', traceback.format_exc(1))

    def explorer(self):
        tree = self.view.view
        for bucket in self.model.buckets():
			try:
				tree.insert('', 'end', bucket, text=bucket, tags=bucket, image=self.folder_img)
				tree.tag_bind(bucket, '<<TreeviewSelect>>', callback=self.fill)
				tree.insert(bucket, 'end', text='')
			except:
				logging.exception('')
        return self

    def _fill(self, item):
        tree = self.view.view
        tree.delete(*tree.get_children(item))
        for n, obj in enumerate(self.model.objects(item), 1):
            # logging.debug(obj)
            tree.insert(item, 'end',
                        text=str(n),
                        values=(
                            obj['filename'],
                            # obj['md5'],
                            # byte2N(obj['chunkSize']),
                            datetime.fromtimestamp(obj['uploadDate'] / 1000),
                            self.byte2N(obj['length'])),
                        tags='DATA')
            tree.tag_bind('DATA', '<Double-Button-1>', callback=self.download)

    def fill(self, _=None):
        """show files when select tree node"""
        item = self.view.view.focus()
        if len(item) > 0:
            self._fill(item)

if __name__ == '__main__':
    w = Controller()
    w.explorer().run()
