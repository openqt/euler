# -*- coding: utf-8 -*-
"""
calculate md5 and sha1 for all files
"""

import hashlib
import logging, sys, os
from datetime import datetime
from sqlalchemy import *
from sqlalchemy.orm import *
from sqlalchemy.types import *
from sqlalchemy.ext.declarative import declarative_base

FILENAME_LEN = 1024

BaseModel = declarative_base()
class TFILEINFO(BaseModel):
    __tablename__ = 'TFILEINFO'

    filename = Column(Unicode(FILENAME_LEN, convert_unicode=True), primary_key=True)
    size = Column(Integer, nullable=False)
    ctime = Column(DateTime)
    mtime = Column(DateTime)
    md5 = Column(String(32), index=True, nullable=False)
    sha1 = Column(String(40), index=True)
    def __init__(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

class FileInfo(object):
    """file has information"""

    def __init__(self, dbname=None):
        if not dbname: dbname = './fileinfo.db'
        self.engine = create_engine('sqlite:///{0}'.format(dbname), echo=True)
        self.session = scoped_session(sessionmaker(self.engine))

        logging.basicConfig(level=logging.DEBUG)
        self.log = logging.getLogger()

    def __filehash(self, filename):
        with open(filename, 'rb') as f:
            md5 = hashlib.md5()
            sha1 = hashlib.sha1()
            while 1:
                bytes = f.read(8 * 1024)
                if bytes:
                    md5.update(bytes)
                    sha1.update(bytes)
                else: break
        return md5.hexdigest(), sha1.hexdigest()

    def info(self, filename):
        """extract file information"""
        self.log.info(filename)
        try:
            code1, code2 = self.__filehash(filename)
            if len(filename) > FILENAME_LEN:
                filename = filename[len(filename) - FILENAME_LEN:]
            stat = os.stat(filename)
            data = {
                'filename': unicode(filename, 'GBK', 'ignore'),
                'ctime': datetime.fromtimestamp(stat.st_ctime),
                'mtime': datetime.fromtimestamp(stat.st_mtime),
                'size': stat.st_size,
                'md5': code1,
                'sha1': code2,
                }
            return data
        except:
            self.log.exception('')
        return None

    def search(self, path):
        """search all files under path"""
        self.log.info(path)

        BaseModel.metadata.create_all(self.engine)
        for root, dirs, files in os.walk(path):
            try:
                for name in files:
                    data = self.info(os.path.join(root, name))
                    if data:
                        self.session.merge(TFILEINFO(**data))
                self.session.commit()
            except:
                self.session.rollback()
                self.log.exception('')


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', default='.', type=str, help='search start point')
    parser.add_argument('--file', default=None, type=str, help='database file location')
    args = parser.parse_args()

    fi = FileInfo(args.file)
    fi.search(args.path)
