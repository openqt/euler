# -*- encoding: utf-8 -*-
import unittest
import logging, sys


def Parameterized(*args):
    def _param(func):
        def __deco(self):
            for i in args:
                log.info('loop for {0}'.format(i))
                func(self, i)
        return __deco
    return _param


class Test(unittest.TestCase):
    def setUp(self):
        log.info('setUp')

    def tearDown(self):
        log.info('tearDown')

    @Parameterized(*'abcd')
    def testA(self, arg):
        log.info('in test A: ' + arg)


    @Parameterized(1, 2, 3, 4)
    def testB(self, arg):
        pass


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, stream=sys.stderr)
    setattr(sys.modules[__name__], 'log', logging.getLogger())
    unittest.main()
