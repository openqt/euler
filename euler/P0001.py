"""
Multiples of 3 and 5
Problem 1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""

calc = lambda x, y: x + y

n = reduce(calc, range(3, 1000, 3))
n += reduce(calc, range(5, 1000, 5))
n -= reduce(calc, range(15, 1000, 15))

print n  # 233168

