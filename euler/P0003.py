"""
Largest prime factor
Problem 3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""

import math

num = 600851475143
n = int((math.sqrt(num)))

slots = [1] * (n / 2)
for i in range(1, len(slots)):
    if slots[i]:  # number = i*2+1
        # val = i*2+1
        val = (i * 2 + 1)
        for j in range(val * 3, n, val * 2):
            slots[(j - 1) / 2] = 0

prime = [2]
for i in range(1, len(slots)):
    if slots[i]:
        prime.append(i * 2 + 1)
print 'Total prime: {0}'.format(len(prime))
# print prime

for i, v in enumerate(prime[::-1]):
    if num % v == 0:
        print 'Iterated: {0}, largest factor is: {1}'.format(i, v)
        break
else:
    print 'Not found.'
