# -*-encoding:utf-8-*-
"""
Largest palindrome product
Problem 4

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""


def palindrome(n):
    s = str(n)
    n = len(s)
    for i in range(n / 2):
        if s[i] != s[-i - 1]:
            return False
    else:
        return True


def find():
    a = b = m = 0
    for i in range(999, 99, -1):
        for j in range(999, i, -1):
            val = i * j
            if val < m:
                break

            if palindrome(val):
                a = i
                b = j
                m = val
    return a, b


a, b = find()
print '{0} x {1} = {2}'.format(a, b, a * b)
