# -*-encoding:utf-8-*-
"""
Smallest multiple
Problem 5

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
"""


def a_to_10():
    def div(n):
        for i in [10, 9, 8, 7]:
            if n % i != 0:
                return False
        return True

    b = i = 2 * 3 * 5 * 7
    while 1:
        if div(i):
            return i
        i += b


def a_to_20():
    def div(n):
        for i in range(11, 21):
            if n % i != 0:
                return False
        return True

    b = i = 2 * 3 * 5 * 7 * 11 * 13 * 17 * 19
    while 1:
        if div(i):
            return i
        i += b


print a_to_10()
print a_to_20()
