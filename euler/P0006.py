# -*-encoding:utf-8-*-
"""
SSum square difference
Problem 6

The sum of the squares of the first ten natural numbers is,

12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 ? 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
"""

s1 = reduce(lambda x, y: x + y ** 2, range(1, 101))
s2 = reduce(lambda x, y: x + y, range(1, 101))

n = 100
print (2 * n + 1) * (n + 1) * n / 6, s1
print n * (n + 1) / 2, s2

print s2 ** 2 - s1
