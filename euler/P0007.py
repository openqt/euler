# -*-encoding:utf-8-*-
"""
10001st prime
Problem 7
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""

n = 200000
slots = [1] * (n / 2)
for i in range(1, len(slots)):
    if slots[i]:  # number = i*2+1
        # val = i*2+1
        val = (i * 2 + 1)
        for j in range(val * 3, n, val * 2):
            slots[(j - 1) / 2] = 0

c = 0
for i in range(0, len(slots)):
    if slots[i]:
        c += 1
        if c == 10001:
            print i * 2 + 1
