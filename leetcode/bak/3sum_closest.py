'''
3Sum Closest

Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

    For example, given array S = {-1 2 1 -4}, and target = 1.

    The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
'''


class Solution:

    # @return an integer
    def threeSumClosest(self, num, target):
        num.sort()
        diff = 10 ** 8
        ret = diff
        for i in range(len(num)):
            start = i + 1
            end = len(num) - 1
            while start < end:
                val = num[start] + num[i] + num[end]
                tmp = abs(target - val)
                if tmp < diff:
                    diff = tmp
                    ret = val
                if val == target:
                    return val
                elif val < target:
                    start += 1
                else:
                    end -= 1
        return ret

if __name__ == '__main__':
    s = Solution()
    assert s.threeSumClosest([-1, 2, 1, -4], 1) == 2
    assert s.threeSumClosest([1, 1, 1, 1], 0) == 3
    assert s.threeSumClosest([1, 1, 1, 1], -100) == 3
    print 'PASS'
