#-*-encoding:utf-8-*-
'''
4Sum

Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target? Find all unique quadruplets in the array which gives the sum of target.

Note:

    Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a �� b �� c �� d)
    The solution set must not contain duplicate quadruplets.

    For example, given array S = {1 0 -1 0 -2 2}, and target = 0.

    A solution set is:
    (-1,  0, 0, 1)
    (-2, -1, 1, 2)
    (-2,  0, 0, 2)
'''


class Solution:

    # @return a list of lists of length 4, [[val1,val2,val3,val4]]
    def fourSum(self, num, target):
        table = {}
        for i, a in enumerate(num):
            for j, b in enumerate(num[i + 1:], i + 1):
                table.setdefault(a + b, []).append((i, j))

        values = set()
        for a in table:
            b = target - a
            if b in table:
                map(values.add, [tuple(sorted((num[v1], num[v2], num[v3], num[v4]))) 
                    for v1, v2 in table[b] for v3, v4 in table[a] if len(set((v1, v2, v3, v4))) == 4])
        return map(list, values)

if __name__ == '__main__':
    s = Solution()
    assert s.fourSum([1, 0, -1, 0, -2, 2], 0) == [[-1, 0, 0, 1], [-2, -1, 1, 2], [-2, 0, 0, 2]]
    print 'PASS'
