'''
Best Time to Buy and Sell Stock

Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.
'''

class Solution:

    # @param prices, a list of integer
    # @return an integer
    def maxProfit(self, prices):
        if len(prices) < 2:
            return 0

        profit = 0
        low = prices[0]
        for price in prices[1:]:
            low = min(low, price)
            profit = max(profit, price - low)

        return profit

if __name__ == '__main__':
    s = Solution()
    assert s.maxProfit([1, 2]) == 1
    print 'PASS'
