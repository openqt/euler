'''
Best Time to Buy and Sell Stock II

Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times). However, you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
'''


class Solution:

    # @param prices, a list of integer
    # @return an integer
    def maxProfit(self, prices):
        profit = 0
        for i in range(1, len(prices)):
            val = prices[i] - prices[i - 1]
            if val > 0:
                profit += val

        return profit

if __name__ == '__main__':
    s = Solution()
    assert s.maxProfit([1, 2]) == 1
    print 'PASS'
