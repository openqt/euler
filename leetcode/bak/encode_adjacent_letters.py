'''
Encode Adjacent Letters

Encode a string by counting the consecutive letter.

(i.e., "aaaabbxxxyyz" might become "a4b2x3y2z1").

'''


class Solution:

    # @param s, letters as string
    # @return an encoded string
    def encode(self, s):
        if not s or len(s) == 0:
            return ""

        val = ""
        prev = s[0]
        count = 1
        for c in s[1:]:
            if c == prev:
                count += 1
            else:
                val += prev + str(count)
                prev = c
                count = 1
        val += prev + str(count)
        return val

if __name__ == '__main__':
    s = Solution()
    assert s.encode("") == ""
    assert s.encode("aaaabbxxxyyz") == "a4b2x3y2z1"
    assert s.encode("aaaaaaaaaaab") == "a11b1"
    print 'PASS'
