'''
Evaluate Reverse Polish Notation

Evaluate the value of an arithmetic expression in Reverse Polish Notation.

Valid operators are +, -, *, /. Each operand may be an integer or another expression.

Some examples:

  ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
  ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6

'''


class Solution:

    # @param tokens, a list of string
    # @return an integer
    def evalRPN(self, tokens):
        q = []
        for token in tokens:
            try:
                q.append(int(token))
            except ValueError:
                a, b = q.pop(), q.pop()
                if token == '+':
                    v = b + a
                elif token == '-':
                    v = b - a
                elif token == '*':
                    v = b * a
                elif token == '/':
                    if (a < 0 and b > 0) or (a > 0 and b < 0):
                        v = -(abs(b) / abs(a))
                    else:
                        v = b / a
                q.append(v)

        return q.pop()

if __name__ == '__main__':
    s = Solution()
    assert s.evalRPN(["2", "1", "+", "3", "*"]) == 9
    assert s.evalRPN(["4", "13", "5", "/", "+"]) == 6
    print 'PASS'
