'''
Insertion Sort List

Sort a linked list using insertion sort.
'''
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:

    # @param head, a ListNode
    # @return a ListNode
    def insertionSortList(self, head):
        if not head or not head.next:
            return head
        cur = dummy = ListNode(0)

        while head:
            if not cur.next or (head.val < cur.next.val):
                cur = dummy

            while cur.next and cur.next.val < head.val:
                cur = cur.next
            x = cur.next
            cur.next = head
            head = head.next
            cur.next.next = x

        return dummy.next
