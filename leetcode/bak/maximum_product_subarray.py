'''
Maximum Product Subarray

Find the contiguous subarray within an array (containing at least one number) which has the largest product.

For example, given the array [2,3,-2,4],
the contiguous subarray [2,3] has the largest product = 6. 
'''


class Solution:

    # @param A, a list of integers
    # @return an integer
    def maxProduct(self, A):
        if not A:
            return 0

        maxv = minv = prod = A[0]
        for i in A[1:]:
            a = maxv * i
            b = minv * i

            maxv = max(max(a, b), i)
            minv = min(min(a, b), i)
            prod = max(maxv, prod)

        return prod

if __name__ == '__main__':
    s = Solution()
    assert s.maxProduct([2, 3, -2, 4]) == 6
    assert s.maxProduct([-2]) == -2
    assert s.maxProduct([0, 2]) == 2
    assert s.maxProduct([-2, 3, -4]) == 24
    print 'PASS'
