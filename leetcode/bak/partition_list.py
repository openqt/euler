'''
Partition List

Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

For example,
Given 1->4->3->2->5->2 and x = 3,
return 1->2->2->4->3->5. 
'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:

    # @param head, a ListNode
    # @param x, an integer
    # @return a ListNode
    def partition(self, head, x):
        p1 = less = ListNode(0)
        p2 = more = ListNode(0)
        p = head
        while p:
            if p.val < x:
                p1.next = p
                p1 = p
            else:
                p2.next = p
                p2 = p
            p = p.next

        p1.next = more.next
        p2.next = None

        return less.next

if __name__ == '__main__':
    from helper.nodes import *
    links = cvtToListNode('143252')
    printListNode(links)

    s = Solution()
    neworder = s.partition(links, '3')
    printListNode(neworder)
