#-*-encoding:utf-8-*-
'''
Reorder List

Given a singly linked list L: L0→L1→…→Ln-1→Ln,
reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…

You must do this in-place without altering the nodes' values.

For example,
Given {1,2,3,4}, reorder it to {1,4,2,3}. 
'''
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:

    def findMiddle(self, head):
        # split the linked list
        c = p1 = p2 = head
        while p2 and p2.next:
            c = p1
            p1 = p1.next
            p2 = p2.next.next
        # now p1 in the middle of whole linked list
        c.next = None
        return p1

    def reverseList(self, head):
        # reverse 2nd half
        p = head.next
        head.next = None
        while p:
            t = head
            head = p
            p = p.next
            head.next = t
        return head

    def intervalList(self, head, head2):
        p1, p2 = head, head2
        while p1.next and p2:
            t = p1.next
            p1.next = p2
            p2 = p2.next
            p1 = p1.next
            p1.next = t
            p1 = p1.next
        if p2:
            p1.next = p2
        return head

    # @param head, a ListNode
    # @return nothing
    def reorderList(self, head):
        if not head or not head.next:
            return

        head2 = self.reverseList(self.findMiddle(head))
        if head != head2:
            self.intervalList(head, head2)

if __name__ == '__main__':
    from helper.nodes import *
    s = Solution()
    h = cvtToListNode([2, 3, 4, 5, 6, 7])
    printListNode(h)
    s.reorderList(h)
    printListNode(h)
