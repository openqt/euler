'''
Rotate Image

You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

Follow up:
Could you do this in-place?

'''


class Solution:

    # @param matrix, a list of lists of integers
    # @return a list of lists of integers
    def rotate(self, matrix):
        return map(list, zip(*matrix[::-1]))

if __name__ == '__main__':
    s = Solution()
    assert s.rotate([list('1234'), list('4567'), list('6789'), list('abcd')]) == [
        ['a', '6', '4', '1'], ['b', '7', '5', '2'], ['c', '8', '6', '3'], ['d', '9', '7', '4']]
    print 'PASS'
