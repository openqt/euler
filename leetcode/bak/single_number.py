'''
Single Number

Given an array of integers, every element appears twice except for one. Find that single one.

Note:
Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory? 
'''


class Solution:

    # @param A, a list of integer
    # @return an integer
    def singleNumber(self, A):
        val = 0
        for i in A:
            val ^= i
        return val

if __name__ == '__main__':
    s = Solution()
    print s.singleNumber([2, 3, 4, 5, 6, 2, 3, 4, 6])
