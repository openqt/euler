'''
Single Number II

Given an array of integers, every element appears three times except for one. Find that single one.

Note:
Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory? 
'''


class Solution:

    # @param A, a list of integer
    # @return an integer
    def singleNumber(self, A):
        one, two = 0, 0
        for i in A:
            two |= i & one
            one ^= i

            t = ~(one & two)
            one &= t
            two &= t
        return one

if __name__ == '__main__':
    s = Solution()
    print s.singleNumber([2, 3, 4, 7, 2, 3, 4, 2, 3, 4])
    print s.singleNumber([-2, -2, 1, 1, -3, 1, -3, -3, -4, -2])
    print s.singleNumber([1, 1, 1, 1])
