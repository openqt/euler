'''
Sort List

Sort a linked list in O(n log n) time using constant space complexity.
'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None


class Solution:

    def merge(self, a, b):
        h = ListNode(-1)
        ph, pa, pb = h, a, b
        while pa and pb:
            if pa.val < pb.val:
                ph.next = pa
                pa = pa.next
            else:
                ph.next = pb
                pb = pb.next
            ph = ph.next
        if pa:
            ph.next = pa
        if pb:
            ph.next = pb
        return h.next

    # @param head, a ListNode
    # @return a ListNode
    def sortList(self, head):
        if not (head and head.next):
            return head

        slow, fast = head, head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
        head2 = slow.next
        slow.next = None
        a = self.sortList(head)
        b = self.sortList(head2)
        return self.merge(a, b)
