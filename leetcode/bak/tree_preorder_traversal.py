'''
Binary Tree Preorder Traversal

Given a binary tree, return the preorder traversal of its nodes' values.

For example:
Given binary tree {1,#,2,3},

   1
    \
     2
    /
   3

return [1,2,3].

Note: Recursive solution is trivial, could you do it iteratively?
'''
# Definition for a  binary tree node
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution:

    # @param root, a tree node
    # @return a list of integers
    def preorderTraversal(self, root):
        ints = []

        stack = []
        while root or stack:
            if root:
                ints.append(root.val)
                stack.append(root)
                root = root.left
            else:
                root = stack.pop()
                root = root.right
        return ints

    def preorderTraversal(self, root):
        self.ints = []
        self.preorderTraversal(root)
        return self.ints
        
    def preorderRecursive(self, root):
        if root:
            self.ints.append(root.val)
            self.preorderTraversal(root.left)
            self.preorderTraversal(root.right)

if __name__ == '__main__':
    s = Solution()
    #assert s.preorderTraversal('') == []
    #assert s.preorderTraversal('213#4') == [2, 1, 4, 3]
