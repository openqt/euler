'''
Valid Sudoku

Determine if a Sudoku is valid, according to: Sudoku Puzzles - The Rules.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'.

53..7....
6..195...
.98....6.
8...6...3
4..8.3..1
7.3323336
.6....28.
...419..5
....8..79

A partially filled sudoku which is valid.

Note:
A valid Sudoku board (partially filled) is not necessarily solvable. Only the filled cells need to be validated. 
'''


class Solution:

    # @param board, a 9x9 2D array
    # @return a boolean
    def isValidSudoku(self, board):
        for i in range(9):
            if not self.isValid(self.rows(board, i)):
                return False
            if not self.isValid(self.cols(board, i)):
                return False
            if not self.isValid(self.cell(board, i)):
                return False
        return True

    def isValid(self, cells):
        checker = [True] * 9
        for c in cells:
            if c <= '9' and c >= '1':
                i = ord(c) - ord('1')
                if checker[i]:
                    checker[i] = False
                else:
                    return False
        return True

    def rows(self, board, row):
        for i in range(9):
            yield board[row][i]

    def cols(self, board, col):
        for i in range(9):
            yield board[i][col]

    def cell(self, board, no):
        mid = [(1, 1), (1, 4), (1, 7),
               (4, 1), (4, 4), (4, 7),
               (7, 1), (7, 4), (7, 7), ]
        n = mid[no]
        for i in range(-1, 2):
            for j in range(-1, 2):
                yield board[n[0] + i][n[1] + j]

if __name__ == '__main__':
    s = Solution()
    assert not s.isValidSudoku(["..4...63.", ".........", "5......9.", "...56....",
                                "4.3.....1", "...7.....", "...5.....", ".........", "........."])
    assert not s.isValidSudoku([".........", ".........", ".9......1", "8........",
                                ".99357...", ".......4.", "...8.....", ".1....4.9", "...5.4..."])
    print("PASS")
