'''
Word Break

Given a string s and a dictionary of words dict, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

For example, given
s = "leetcode",
dict = ["leet", "code"].

Return true because "leetcode" can be segmented as "leet code". 
'''


class Solution:

    def __init__(self):
        self.t = set()

    # @param s, a string
    # @param dict, a set of string
    # @return a boolean
    def wordBreak(self, s, dict):
        if not s:
            return True
        for i in range(1, len(s) + 1):
            if s[:i] in dict:
                if s[i:] not in self.t:
                    if self.wordBreak(s[i:], dict):
                        return True
                    else:
                        self.t.add(s[i:])
        return False

if __name__ == '__main__':
    s = Solution()
    print s.wordBreak('leetcode', {'leet', 'code'})
