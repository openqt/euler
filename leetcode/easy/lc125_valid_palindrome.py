'''
Valid Palindrome

Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

For example,
"A man, a plan, a canal: Panama" is a palindrome.
"race a car" is not a palindrome.

Note:
Have you consider that the string might be empty? This is a good question to ask during an interview.

For the purpose of this problem, we define empty string as valid palindrome.
'''

class Solution:
    # @param {string} s
    # @return {boolean}
    def isPalindrome(self, s):
        a = 0; b = len(s) - 1
        while a < b:
            if not self.isAlphanumeric(s[a]):
                a += 1
            elif not self.isAlphanumeric(s[b]):
                b -= 1
            elif s[a].lower() != s[b].lower():
                return False
            else:
                a += 1; b -= 1
        return True

    def isAlphanumeric(self, c):
        c = c.lower()
        return 'a' <= c <= 'z' or '0' <= c <= '9'


if __name__ == '__main__':
    s = Solution()
    assert s.isPalindrome("A man, a plan, a canal: Panama")
    assert not s.isPalindrome("race a car")
    assert not s.isPalindrome("1a2")
    print 'PASS'
