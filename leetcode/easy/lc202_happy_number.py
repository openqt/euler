# -*- encoding: utf-8 -*-
'''
Happy Number

Write an algorithm to determine if a number is "happy".

A happy number is a number defined by the following process: Starting with any positive integer, replace the number by the sum of the squares of its digits, and repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1. Those numbers for which this process ends in 1 are happy numbers.

Example: 19 is a happy number

12 + 92 = 82
82 + 22 = 68
62 + 82 = 100
12 + 02 + 02 = 1
Credits:
Special thanks to @mithmatt and @ts for adding this problem and creating all test cases.
'''


class Solution:
    # @param {integer} n
    # @return {boolean}
    def isHappy(self, n):
        steps = set()
        while n != 1:
            n = sum(int(i)**2 for i in str(n))
            if n in steps: return False
            steps.add(n)
        return True

if __name__ == '__main__':

    s = Solution()
    assert s.isHappy(19)
    assert not s.isHappy(20)
    print 'PASS'
