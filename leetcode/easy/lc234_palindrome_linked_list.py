# -*- encoding: utf-8 -*-
'''
Palindrome Linked List

Given a singly linked list, determine if it is a palindrome.

Follow up:
Could you do it in O(n) time and O(1) space?
'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# 1 > 2 > 3 > 2 > 1
class Solution:
    # @param {ListNode} head
    # @return {boolean}
    def isPalindrome2(self, head):
        if not head or not head.next: return True

        fast = slow = head
        stack = [slow]
        while fast.next and fast.next.next:
            fast = fast.next.next
            slow = slow.next
            stack.append(slow)  # push to stack

        if not fast.next:
            stack.pop()  # remove the middle one

        slow = slow.next
        while len(stack) > 0 and slow:
            if stack.pop().val != slow.val:
                return False
            slow = slow.next
        return slow is None

    def isPalindrome(self, head):
        if not head or not head.next: return True

        fast = slow = head
        while fast.next and fast.next.next:
            fast = fast.next.next
            slow = slow.next

        node = ListNode(0)
        slow = slow.next
        while slow:
            n = slow
            slow = n.next

            t = node.next
            node.next = n
            n.next = t

        while head and node.next:
            if head.val != node.next.val: return False
            head = head.next
            node = node.next
        return True

if __name__ == '__main__':
    from ListNode import *

    s = Solution()
    print s.isPalindrome(toListNode('12321'))
    print s.isPalindrome(toListNode('123321'))
    print not s.isPalindrome(toListNode('12322'))
    print 'PASS'
