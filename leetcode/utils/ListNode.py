# Definition for singly-linked node
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


def toListNode(seq):
    p = head = ListNode(0)

    for i in seq:
        p.next = ListNode(i)
        p = p.next
    return head.next


def show(node):
    p = node

    s = ''
    while p:
        s += '{} -> '.format(p.val)
        p = p.next
    s += 'END'
    print s


# if __name__=='__main__':
#     show(toListNode('abcdefg'))
