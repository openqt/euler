# Definition for reverse polish notation
PRIORITY = {
    '+': 10,
    '-': 10,
    '*': 20,
    '/': 20,
    '(': 0,
}

def calculate(s):
    return evalRNP(toRPN(s))

def toRPN(s):
    rpn = []
    stack = []

    pos = 0
    while pos < len(s):
        val, pos = _next(s, pos)
        if isinstance(val, int):
            rpn.append(val)
        elif val == '(':
            stack.append(val)
        elif val == ')':
            while True:
                n = stack.pop()
                if n == '(':
                    break
                rpn.append(n)
        elif val in PRIORITY:
            pri = PRIORITY[val]
            while len(stack) > 0:
                n = stack[-1]
                if PRIORITY[n] < pri:
                    break
                rpn.append(stack.pop())
            stack.append(val)
        # else:
        #     print '>>>', val, pos
    rpn.extend(stack[::-1])
    return rpn

def _next(s, pos):
    t = ''
    while pos < len(s) and '0' <= s[pos] <= '9':
        t += s[pos]
        pos += 1
    if len(t) > 0:
        return int(t), pos
    else:
        pos += 1
        return s[pos-1], pos

def evalRNP(rpn):
    stack = []
    for i in rpn:
        if isinstance(i, int):
            stack.append(i)
        else:
            b, a = stack.pop(), stack.pop()
            if i == '+':
                stack.append(a + b)
            elif i == '-':
                stack.append(a - b)
            elif i == '*':
                stack.append(a * b)
            elif i == '/':
                stack.append(a / b)
            else:
                pass
            # print a,i,b
    return stack[0]


if __name__ == '__main__':
    print calculate('2 + 3 * 4 - (7 - 2) * 3') == -1
