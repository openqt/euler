"""
OJ's Binary Tree Serialization:
The serialization of a binary tree follows a level order traversal, where '#' signifies a path terminator where no node exists below.

Here's an example:
   1
  / \
 2   3
    /
   4
    \
     5
The above binary tree is serialized as "{1,2,3,#,#,4,#,#,5}".
"""

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

    def __repr__(self):
        return repr(self.val)


def toTreeNode(seq):
    if len(seq) == 0: return None

    head = TreeNode(seq[0])
    Q = [head]
    n = 0
    while n < len(seq):
        node = Q.pop(0)

        n += 1
        if n >= len(seq): break
        if seq[n] != '#':
            node.left = TreeNode(seq[n])
            Q.append(node.left)

        n += 1
        if n >= len(seq): break
        if seq[n] != '#':
            node.right = TreeNode(seq[n])
            Q.append(node.right)

    return head


def preorder(node, seq=None):
    if seq is None: seq = []
    if node is not None:
        seq.append(node.val)
        preorder(node.left, seq)
        preorder(node.right, seq)
    return seq

# def show(node):
#     seq = preorder(node)
#     print seq
#     print '-' * 30

def show(node, s=False):
    val = []

    Q = [node]
    while Q:
        q = Q; Q = []
        for i in q:
            if i:
                Q.append(i.left); Q.append(i.right)
                val.append(i.val)
            else:
                val.append('#')
    while val[-1] == '#':
        val.pop()

    if s:
        print ''.join(val)
    else:
        print val

def shows(node):
    show(node, True)

if __name__=='__main__':
    # show(toTreeNode([1,2,3,'#','#',4,'#','#',5]))
    # show(toTreeNode([3,9,20,'#','#',15,7]))
    # show(toTreeNode('12345'))
    pass
